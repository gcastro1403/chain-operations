
public class DivideNumbers implements Chain{
	
	private Chain nextChain;
	
	@Override
	public void setNextChain(Chain setNextChain) {
		this.nextChain = setNextChain;
		
	}

	@Override
	public int calculate(Numbers request) {
		if("div".equals(request.getOperation())) {
			return request.getNumber1()/ request.getNumber2();
		}
		return Integer.MAX_VALUE;
	}

}
