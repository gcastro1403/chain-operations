import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class TestChain {
	
	static Chain chainCalc1 = new AddNumbers();
	static Chain chainCalc2 = new SubNumbers();
	static Chain chainCalc3 = new MultNumbers();
	static Chain chainCalc4 = new DivideNumbers();
	
	@BeforeAll
	public static void setup() {
		chainCalc1.setNextChain(chainCalc2);
		chainCalc2.setNextChain(chainCalc3);
		chainCalc3.setNextChain(chainCalc4);
		
	}
	
	
	@ParameterizedTest
	@MethodSource("provideNumbers")
	void Chaintest(int number1, int number2, String cmd, int expect) {
		Numbers request = new Numbers(number1,number2,cmd);
		
		assertTrue(chainCalc1.calculate(request) == expect);
		
	}
	
	private static Stream<Arguments> provideNumbers(){
		return Stream.of(
					Arguments.of(8,2,"add",10),
					Arguments.of(7,2,"sub",5),
					Arguments.of(3,6,"mult",18),
					Arguments.of(4,2,"div",2)
				);
	}

}
